using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TouchInput : MonoBehaviour
{
 [SerializeField] private GameObject ball;
 [SerializeField] private Rigidbody2D pivotRigidBody;
 [SerializeField] private float detachDelay = 0.15f;
 [SerializeField] private float respawnDelay = 5.0f;

 private GameObject currentBall;

 private Rigidbody2D ballRigidBody;

 private SpringJoint2D ballSpringJoint;
     
    private Camera mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
       // SpawnBall();
    }

    // Update is called once per frame
    void Update()
    {
        if (!Touchscreen.current.primaryTouch.press.IsPressed())
        {
            return;
        }

        Vector3 touchPosition = Touchscreen.current.primaryTouch.position.ReadValue();
        Vector3 worldPosition = mainCamera.ScreenToWorldPoint(touchPosition);

        Debug.Log("TOUCH POSITION: " + touchPosition);
        Debug.Log("WORLD POSITION: " + worldPosition);
    }
}
